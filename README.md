# Make the world safe for ASCII

> **(May 2021)** moved to [md0.org/asciifold](https://md0.org/asciifold). Use `import md0.org/asciifold`

Go library to fold unicode into ASCII.  Used to create lowest common
denominator slugs in [package slugify](https://md0.org/slugify).

## Summary from the project this package is based on:

An Golang port of the Apache Lucene ASCII Folding Filter. This library
converts alphabetic, numeric, and symbolic Unicode characters into their
ASCII equivalents, if one exists.

(see file README.orig.md for the original README)
