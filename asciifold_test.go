package asciifold

import "testing"

func TestTextFolding(t *testing.T) {
	type Case struct {
		Expected string
		Input    string
	}

	cases := []Case{
		Case{"Some", "Söme"},
		Case{"Gum", "Güm"},
		Case{"Cunku", "Çünkü"},
		Case{"ocsiguOCSIGU", "öçşiğüÖÇŞİĞÜ"},
		Case{"alemi huzun icundur", "âlêmî hüzün içûndur"},
		Case{"Passe compose", "Passé composé"},
		Case{"En reponse a votre annonce dans le magazine FUSAC le 20 juillet 2010, je vous ecris pour poser ma candidature pour le poste de enseignante remplacant d'anglais dans votre ecole.  Puisque je m'interesse beaucoup a l'enseignement des langues, ce poste correspond parfaitement a mes objectifs professionnels.", "En réponse à votre annonce dans le magazine FUSAC le 20 juillet 2010, je vous écris pour poser ma candidature pour le poste de enseignante remplaçant d’anglais dans votre école.  Puisque je m’intéresse beaucoup à l’enseignement des langues, ce poste correspond parfaitement à mes objectifs professionnels."},
		Case{"Assistante d'anglais dans des ecoles primaires en France pendant deux ans (Academie de Paris en 2007-2008 et Academie de Creteil en 2009-2010), j'ai egalement cree un cours intensif de langue francaise pour des enfants ages de 4 a 9 ans dans le Massachusetts en 2008.  Puisque j'emploie beaucoup de methodologies diverses dans l'organisation de mes cours, ces eleves ont appris par coeur des poemes tels que \" Sonnet 116 \" de Shakespeare et \" Dreams \" de Langston Hughes, et ils ont chante \" Imagine \" des Beatles lors de leur spectacle de fin d'annee.  Dans les petites classes, l'usage de la chanson et du jeu ont ete d'une importance capitale pour engager les eleves et leur donner envie d'apprendre.", "Assistante d’anglais dans des écoles primaires en France pendant deux ans (Académie de Paris en 2007-2008 et Académie de Créteil en 2009-2010), j’ai également créé un cours intensif de langue française pour des enfants âgés de 4 à 9 ans dans le Massachusetts en 2008.  Puisque j’emploie beaucoup de méthodologies diverses dans l’organisation de mes cours, ces élèves ont appris par cœur des poèmes tels que « Sonnet 116 » de Shakespeare et « Dreams » de Langston Hughes, et ils ont chanté « Imagine » des Beatles lors de leur spectacle de fin d’année.  Dans les petites classes, l’usage de la chanson et du jeu ont été d’une importance capitale pour engager les élèves et leur donner envie d’apprendre."},
		Case{"Cigdem cicek", "Çiğdem çiçek"},
		// Case{":", "："},
		Case{"!\"#$%&'()*+,-./0123456789;<=>?@", "！＂＃＄％＆＇（）＊＋，－．／０１２３４５６７８９；＜＝＞？＠"},
		Case{"ABCDEFGHIJKLMNOPQRSTUVWXYZ", "ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ"},
		//Case{"`", "｀"},
		//Case{"|", "｜"},
		//Case{"", "｡｢｣､･"},
		Case{"[\\]^_abcdefghijklmnopqrstuvwxyz{}~", "［＼］＾＿ａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ｛｝～"},
		Case{"AE", "Ǣ"},
		Case{"(a)(b)(c)(d)(e)(f)(g)(h)(i)(j)(k)(l)(m)(n)(o)(p)(q)(r)(s)(t)(u)(v)(w)(x)(y)(z)", "⒜⒝⒞⒟⒠⒡⒢⒣⒤⒥⒦⒧⒨⒩⒪⒫⒬⒭⒮⒯⒰⒱⒲⒳⒴⒵"},
		Case{"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", "ⒶⒷⒸⒹⒺⒻⒼⒽⒾⒿⓀⓁⓂⓃⓄⓅⓆⓇⓈⓉⓊⓋⓌⓍⓎⓏⓐⓑⓒⓓⓔⓕⓖⓗⓘⓙⓚⓛⓜⓝⓞⓟⓠⓡⓢⓣⓤⓥⓦⓧⓨⓩ"},
		//Case{"", "ﬅ"},
		Case{"fffiflffifflst", "ﬀﬁﬂﬃﬄﬆ"},
		Case{"1234567891011121314151617181920", "①②③④⑤⑥⑦⑧⑨⑩⑪⑫⑬⑭⑮⑯⑰⑱⑲⑳"},
		Case{"(1)(2)(3)(4)(5)(6)(7)(8)(9)(10)(11)(12)(13)(14)(15)(16)(17)(18)(19)(20)", "⑴⑵⑶⑷⑸⑹⑺⑻⑼⑽⑾⑿⒀⒁⒂⒃⒄⒅⒆⒇"},
		Case{"1.2.3.4.5.6.7.8.9.10.11.12.13.14.15.16.17.18.19.20.", "⒈⒉⒊⒋⒌⒍⒎⒏⒐⒑⒒⒓⒔⒕⒖⒗⒘⒙⒚⒛"},
		Case{"aeDZDzDZDzdzdzIJij LJLjlj lslz NJNjnj OEoe", "ǽǄǅǱǲǳǆĲĳ Ǉǈǉ ʪʫ Ǌǋǌ Œœ"},
		// quote info:
		// http://www.cl.cam.ac.uk/~mgk25/ucs/quotes.html
		// left single quote, right single quote
		Case{"''", "\u2018\u2019"},
		// left double quote, right double quote
		Case{"\"\"", "\u201C\u201D"},
		// ellipsis
		Case{"a...b", "a…b"},
		// some basic stuff, sanity checking
		Case{"123", "123"},
		Case{"", ""},
		Case{"  abc  ", "  abc  "},
	}

	for _, c := range cases {
		folded := Fold(c.Input)
		if c.Expected != folded {
			t.Errorf("Expected %s Got %s\n", c.Expected, folded)
		}
	}
}

// for regression testing
func TestEllipsis(t *testing.T) {
	input := "a…b"
	expected := "a...b"
	folded := Fold(input)
	if folded != expected {
		t.Errorf("Fold(%q) = %s; want %s", input, folded, expected)
	}
}

// https://en.wikipedia.org/wiki/List_of_Unicode_characters#Unicode_symbols
func TestOtherUnicodeSymbols(t *testing.T) {
	tests := []struct {
		input    string
		expected string
		desc     string
	}{
		{"a–b", "a-b", "En dash"},
		{"a—b", "a-b", "Em dash"},
		{"a―b", "a-b", "Horizontal bar"},
		{"a‗b", "a_b", "Double low line"},
		{"a‘b", "a'b", "Left single quotation mark"},
		{"a’b", "a'b", "Right single quotation mark"},
		{"a‚b", "a'b", "Single low-9 quotation mark"},
		{"a‛b", "a'b", "Single high-reversed-9 quotation mark"},
		{"a“b", "a\"b", "Left double quotation mark"},
		{"a”b", "a\"b", "Right double quotation mark"},
		{"a„b", "a\"b", "Double low-9 quotation mark"},
		{"a†b", "a-b", "Dagger"},
		{"a‡b", "a-b", "Double dagger"},
		{"a•b", "a-b", "Bullet"},
		{"a…b", "a...b", "Horizontal ellipsis"},
		{"a′b", "a'b", "Prime"},
		{"a″b", "a\"b", "Double prime"},
		{"a‹b", "a'b", "Single left-pointing angle quotation mark"},
		{"a›b", "a'b", "Single right-pointing angle quotation mark"},
		{"a‼b", "a!!b", "Double exclamation mark"},
		{"a‾b", "a-b", "Overline"},
		{"a⁄b", "a/b", "Fraction slash"},
		// don't know if these are right!
		{"a⁊b", "a-b", "Tironian et sign"},
		{"a‰b", "a%b", "Per mille sign"},
	}

	for _, test := range tests {
		folded := Fold(test.input)
		if folded != test.expected {
			t.Errorf("Fold(%q) = %s; want %s (%s)", test.input, folded, test.expected, test.desc)
		}
	}
}
